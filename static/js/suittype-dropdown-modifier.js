
$(document).ready(function () {
    $("#id_CourtType").change(function () {
        // This event listener was added to check the selection of different option in the CourtType Dropdown.
        // Based on that selection, SuitType options are fetched from the server via ajax request.

        var country_id = $(this).val();
        
        if(country_id == "") {
            // if user selects default/first option which has no value
            // all the options (except first) from the SuitType dropdown are removed.
            var options = $("#id_SuitType option:not(:first)").remove();

        } else {
            // If the user selects a valid option from CourtType dropdown
            // fire an ajax request and fetch the related cities from the server

            var cities_url = window.location.origin + "/admin/cities/" + country_id;

            $.ajax({                       
                url: cities_url,                   
                
                success: function (data) {   
                    $("#id_SuitType").html(data);  
                }

                // add failure case here
            });

        }
    });
    
});
