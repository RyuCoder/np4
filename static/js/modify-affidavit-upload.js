$(document).ready(function () {
    
    var handle = $("#id_document_submitted");
    var affidavit = $("#id_Document");

    function update_affidavit_upload() {
        
        var value = handle.val();
        
        if(value == "N") { //disable affidavit upload
            affidavit.prop("disabled", true);
        } else { //enable affidavit upload
            affidavit.prop("disabled", false);
        }

    }
    
    // This is being called here to update affidavit uploads, at the time of the page load
    update_affidavit_upload();
       
    // This is to update affidavit uploads, when the user selects Yes and No in the dropdown
    handle.change(function() {
        update_affidavit_upload();
    });

});
