from pprint import pprint

from django import forms 
from django.utils import timezone

from newapp.models import Profile, City, Country


class BaseProfileAdminForm(forms.ModelForm):

    class Meta:
        model = Profile
        # They are for example only, Choose anyone here as they will get overwritten
        fields = ['user', 'CourtType', 'SuitType']

    def __init__(self, *args, **kwargs):
        super(BaseProfileAdminForm, self).__init__(*args, **kwargs)
        self._update_suittype_queryset()

    def _update_suittype_queryset(self):
        if self.instance:  # Editing and existing instance
            country = self.fields['CourtType']
            self.fields['SuitType'].queryset = City.objects.filter(country=self.instance.CourtType)

    def clean_CourtType(self):
        court_type = self.cleaned_data['CourtType']

        # This queryset needs to be updated
        # Because, user can select a different CourtType i.e. country.
        # When it happens, its available valid options needs to be updated as per entries in database
        self.fields['SuitType'].queryset = City.objects.filter(country=court_type)
        return court_type

    def clean_SuitType(self):
        court_type = self.cleaned_data['CourtType']
        suit_type = self.cleaned_data['SuitType']
        cities = City.objects.filter(country=court_type).values_list("name", flat=True)

        # Checking for currently selected CourtType, not of the instance
        if suit_type.name not in cities:
            message = "You must select a valid city for the country."
            raise forms.ValidationError(message)

        return suit_type
    
    def clean_CaseNoticeDate(self):
        # This must be a future date
        notice_date = self.cleaned_data['CaseNoticeDate']

        if notice_date <= timezone.now().date():
            message = "This date must be greater than today."
            raise forms.ValidationError(message)

        return notice_date

    def clean_CaseInwardDate(self):
        # This must be a past date
        inward_date = self.cleaned_data['CaseInwardDate']

        if inward_date >= timezone.now().date():
            message = "This date must be less than today."
            raise forms.ValidationError(message)

        return inward_date


class ProfileAdminSuperuserForm(BaseProfileAdminForm):

    class Meta:
        model = Profile
        # fields for super user only
        # fields = [ 'CourtType', 'SuitType', ]
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        # Write a new __init__ as below for the each new for that you extend from BaseProfileAdminForm
        super(ProfileAdminSuperuserForm, self).__init__(*args, **kwargs)
        self._update_suittype_queryset()

