from pprint import pprint
from datetime import datetime

from django.contrib import admin
from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedDropdownFilter, ChoiceDropdownFilter
from django.contrib.auth.models import User

from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter

# from newapp.models import Profile,CourtDecision,CourtCasesAffadavit
from newapp.models import Profile, CourtDecision, CourtCasesAffadavit, Country, City
from newapp.forms import ProfileAdminSuperuserForm


class CountryAdmin(admin.ModelAdmin):

    list_display = ["id", "name"]


class CityAdmin(admin.ModelAdmin):

    list_display = ["id", "name", "country"]


admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)



class CourtCasesAffadavitInline(admin.TabularInline):
    extra = 0
    model = CourtCasesAffadavit


class ProfileAdmin(admin.ModelAdmin):
    
    save_on_top = True
    save_as = True
    inlines = [CourtCasesAffadavitInline, ]
    ordering = ('updated_at', 'CaseNumber',)
    # raw_id_fields = ["user", ]

    # list_display = Profile._meta.get_all_field_names()

    # list_display = ["user","CaseNumber","CaseYear","bio","location","birth_date",] ''
    # print delta.days

    list_display = ('updated_at', 'CaseYear', 'CaseNumber', 'CaseLastDate', 'SuitType', 'CourtType', 'CaseStatus', 'InwardNumber', 'CaseNoticeDate', 'CaseNoticeSubject',
                    'CaseReceiverName', 'CaseInwardDate', 'CaseActionTaken', 'CaseAdvocate', 'CasePartyNames',
                    'CaseOppositePartyNames', 'user'
                    )

    # def Show_No_Of_Days_Left(self,obj):
    #       delta =  obj.CaseLastDate - datetime.now().date()
    #       # print delta.days  ,'Show_No_Of_Days_Left'

    #       return delta

    # search_fields =['CaseNumber','CaseYear','InwardNumber','CaseNoticeDate','CaseNoticeSubject',
    #                   'CaseReceiverName','CaseInwardDate','CaseActionTaken'
    #                ]

    search_fields = ['user', 'CaseNumber', 'CaseYear', 'InwardNumber', 'CaseNoticeDate', 'CaseNoticeSubject',
                     'CaseReceiverName', 'CaseInwardDate', 'CaseActionTaken', 'CaseAdvocate', 'CasePartyNames',
                     'CaseOppositePartyNames', 'CaseLastDate', 'user', 'CaseStatus',
                     'Show_No_Of_Days_Left',
                     ]

    #            # search_fields = ('CaseNumber__field2','CaseYear__field3','bio__field4','location__field5','birth_date_field6',)

    # fieldsets =    [
    #                   ('Location information',
    #                   {'fields': ['State','District','user']  }  ),

    #                   ('Department Internal information', {'classes':['collapse'],'fields': [ ('InwardNumber','CaseInwardDate'),
    #                   ('CaseNoticeSubject','CaseNoticeDate',),('CaseReceiverName','CaseActionTaken') ] } ),

    #                   ('Court Case Information', {'classes':['collapse'],
    #                   'fields': [ ('CourtType','SuitType'),
    #                   ('CaseYear','CaseNumber'),('CaseSubject','CaseSubmittedDate') ] } ),

    #                   ('Party Information', {'classes':['collapse'],
    #                   'fields': [ ('CaseAdvocate'),
    #                   ('CasePartyNames','CaseOppositePartyNames'),'CaseLastDate' ] } ),

    #                   ('Incharge Officer Information', {'classes':['collapse'],
    #                   'fields': [ ('UserNext','UserNextDate'),
    #                   ('UserNextDepartment','CaseStatus') ] } )
    #                ]
    list_per_page = 10
    list_editable = ['CaseAdvocate', 'CasePartyNames',
                     'CaseOppositePartyNames', 'CaseYear', 'CaseLastDate']
    #readonly_fields = ['CaseYear','CaseNumber']
    # ordering = ('CaseNumber','Show_No_Of_Days_Left',)
    ordering = ('-updated_at', 'CaseYear', 'CaseNumber',)

    list_filter = (
        # for ordinary fields
        ('CaseNumber', DropdownFilter),
        ('CaseYear', DropdownFilter),
        # ('SuitType', DropdownFilter),
        ('updated_at', DropdownFilter),
        'user',

        # for choice fields
        # ('a_choicefield', ChoiceDropdownFilter),
        # for related fields
        # ([('CaseNumber', 'CaseYear')], RelatedDropdownFilter),
    )

    normaluser_fields = ['user', 'CourtType', 'SuitType', 'CaseNumber', 'CaseYear', 'CaseAdvocate', 'CasePartyNames',
                         'CaseOppositePartyNames', 'CaseLastDate']
    superuser_fields = ['CaseStatus']
    inward_fields = ['InwardNumber', 'CaseNoticeDate', 'CaseNoticeSubject',
                     'CaseReceiverName', 'CaseInwardDate', 'CaseActionTaken']

    def get_form(self, request, obj=None, **kwargs):


        # check user type here and return a different form based on that         
        # e.g. for Superuser below form is returned 
        # if request.user.is_superuser:
        #     return ProfileAdminSuperuserForm
    
        # So if there are n different scenarios, you will create that many forms
        
        # Also, concept and calculations of normaluser_fields, superuser_fields 
        # and inward_fields needs to be removed from the code

        return ProfileAdminSuperuserForm




        # Old code of get_form() 
        # username = request.user.username

        # print("----- Inside get_exclude Method--------")
        # print(username)

        # # problem with checking username is that if there are 10 usernames
        # # you might end up writing lots of if conditions. 
        # # A better approach is to use concepts of groups here 
        
        # if username == 'dgp':  # if user is not a superuser
        #     self.fields = self.normaluser_fields

        # elif username == 'inumber':
        #     print("-----  ***********  Inside DGP LOGIN ***-------")
        #     self.fields = self.inward_fields

        # else:
        #     self.fields = self.normaluser_fields + \
        #         self.superuser_fields + self.inward_fields

        # form = super(ProfileAdmin, self).get_form(request, obj, **kwargs)

        # return form 

    # CODE work to add only current user in drop down
    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
    #       if db_field.name == 'user':
    #           kwargs['queryset'] = User.objects.filter(username=request.user.username)
    #       return super(ProfileAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


    def get_queryset(self, request):
        qs = super(ProfileAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    class Media: 
        js = ['admin/js/vendor/jquery/jquery.min.js',
              'js/suittype-dropdown-modifier.js']


admin.site.register(Profile, ProfileAdmin)

# --------------------------------------------


class CourtDecisionInline(admin.TabularInline):
    extra = 0
    model = CourtDecision


class CourtCasesAffadavitAdmin(admin.ModelAdmin):
    inlines = [CourtDecisionInline, ]
    list_display = ['updated_at', 'user', 'CaseNumber',  'court_case_response_given', 'document_submitted', 'CasesDocumentsDate',
                    'DocumentName', ]
    search_fields = ("user",  "court_case_response_given",
                     "document_submitted", "CasesDocumentsDate")

    raw_id_fields = ["CaseNumber", ]

    ordering = ('-updated_at', 'CaseNumber',)

    list_filter = (
        # for ordinary fields
        ('CasesDocumentsDate', DateRangeFilter),
        ('updated_at', DateRangeFilter),
        'user',
        # ('CaseYear', DropdownFilter),
        # ('location', DropdownFilter),

    )

    def get_queryset(self, request):
        qs = super(CourtCasesAffadavitAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    class Media:
        js = ['admin/js/vendor/jquery/jquery.min.js',
              'js/modify-affidavit-upload.js']



admin.site.register(CourtCasesAffadavit, CourtCasesAffadavitAdmin)


class CourtDecisionAdmin(admin.ModelAdmin):

    list_display = ['updated_at', 'user', 'CaseNumber', 'action_date', 'decision_taken', 'decision_details',
                    'court_next_date', 'next_expected_decision']

    search_fields = ['court_action', 'user', 'CaseNumber', 'action_date', 'decision_taken', 'decision_details',
                     'court_next_date', 'next_expected_decision']

    raw_id_fields = ["CaseNumber", ]
    list_per_page = 5

    list_filter = (
        # for ordinary fields
        ('action_date', DateRangeFilter),
        ('updated_at', DateRangeFilter),
        'user',
        # ('user_id', DropdownFilter),
        # ('location', DropdownFilter),

    )

    ordering = ('-updated_at', 'CaseNumber',)

    def get_queryset(self, request):
        qs = super(CourtDecisionAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
    #       if db_field.name == "CourtDecision":
    #           kwargs["queryset"] = CourtDecision.objects.filter(user=request.user)
    #       return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(CourtDecision, CourtDecisionAdmin)
