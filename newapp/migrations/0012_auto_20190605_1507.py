# Generated by Django 2.2.1 on 2019-06-05 15:07

import datetime
from django.db import migrations, models
import newapp.models


class Migration(migrations.Migration):

    dependencies = [
        ('newapp', '0011_auto_20190605_1452'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='CaseInwardDate',
            field=models.DateField(blank=True, default=datetime.datetime.now, null=True, validators=[newapp.models.no_future]),
        ),
        migrations.AlterField(
            model_name='profile',
            name='CaseNoticeDate',
            field=models.DateField(blank=True, default=datetime.datetime.now, null=True, validators=[newapp.models.no_future]),
        ),
    ]
