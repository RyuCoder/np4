from django.shortcuts import render

from newapp.models import City 


def get_cities(request, country_id):
    """
        Add authentication and Authorization on this view as applicable.
    """

    cities = City.objects.filter(country_id=country_id).order_by('name')

    return render(request, 'admin/partial_cities.html', {'cities': cities})
