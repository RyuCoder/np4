from datetime import datetime
from django.contrib import admin
# import datetime
from django import forms
from django.db import models
from django.contrib.auth.models import User
# import datetime
from django.utils import timezone
# from compositefk.fields import CompositeForeignKey


# Where are they being used?
# if they are validators, make a separate file called validators.py and put these there.
# class Validators():
# 
# @classmethod
# def validate_date_no_future(self, date):
#     if date > timezone.now().date():
#         raise ValidationError(
#             "Date cannot be in the FUTURE - Please Enter Correct Date")
#
# Put rest of the validators in the same class  
# 
# As these validations are checked in the forms, you may choose to delete these completely.
# As they are not reuqired at all
#  
def validate_date_no_future(date):
    if date > timezone.now().date():
        raise ValidationError(
            "Date cannot be in the FUTURE - Please Enter Correct Date")


def validate_date_no_past(date):
    if date < timezone.now().date():
        raise ValidationError(
            "Date cannot be in the PAST - Please Enter Correct Date")


def no_future(value):
    # today = date.today()
    today = datetime.date.today()
    if value > today:
        raise ValidationError('Date cannot be in the future.')


def no_past(value):
    # today = date.today()
    today = datetime.date.today()

    if value < today:
        raise ValidationError('Date cannot be in the PAST.')


class BaseModel(models.Model):
    # Wherever you need below two fields, extend from this model, instead of models.Model
    # and remove these same fields from those models
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name="CASE Updated Date")

    class Meta: 
        abstract = True 


class Country(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Country"
        verbose_name_plural = "Countries"


class City(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name + " - " + str(self.country)

    class Meta:
        verbose_name = "City"
        verbose_name_plural = "Cities"


class Profile(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name="CASE Updated Date")

    # state and district are predefined by the government.
    # nobody knows all of them and their short forms.
    # They should be in seperate table like Country and City
    # So you would use ForeignKey here to those new models.
    State = models.CharField(max_length=100, default="MH")
    District = models.CharField(max_length=100, default="Parbhani")

    user = models.ForeignKey(User, on_delete=models.CASCADE, default="inumber")

    CourtType = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True)
    SuitType = models.ForeignKey(City, on_delete=models.SET_NULL, null=True)

    InwardNumber = models.CharField(max_length=100, default="2019/3")
    # CaseNoticeDate = models.DateField(null=True, blank=True, default=datetime.now, validators=[no_future])
    CaseNoticeDate = models.DateField(null=True, blank=True, default=datetime.now)   # Must be Future Not Working
    CaseNoticeSubject = models.CharField(max_length=100, default="Inquiry in Department")
    CaseReceiverName = models.CharField(max_length=100, default="Ramakant Lal")
    # CaseInwardDate  = models.DateField(null=True, blank=True, default=datetime.now, validators=[no_future])
    CaseInwardDate = models.DateField(null=True, blank=True, default=datetime.now)    # Must be Past Not Working
    CaseActionTaken = models.CharField(max_length=200, default="Verification is going on")

    CaseYear = models.PositiveIntegerField(default=2019)
    CaseNumber = models.PositiveIntegerField(default=1)
    CaseSubmittedDate = models.DateField(default="2019-01-14")
    CaseSubject = models.CharField(max_length=200, default="Corruption and Dowry")

    CaseAdvocate = models.CharField(max_length=200, default="Advoc VIJAY Dinanathh CHAVHAN")
    CasePartyNames = models.CharField(max_length=200, default="Manish Singh")
    CaseOppositePartyNames = models.CharField(max_length=200, default="Raj Kumar ")
    CaseLastDate = models.DateField(default="2019-05-24")
    CaseStatus = models.CharField(max_length=100, default="Pending")

    class Meta:
        unique_together = (('CaseNumber', 'CaseYear'),)

    def __str__(self):
        return '%s/%s' % (self.CaseNumber, self.CaseYear)

    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"


class CourtCasesAffadavit(models.Model):

    court_case_responded = (
        ("Y", "Yes"),
        ("N", "No")
    )

    reply_mode = (
        ("Y", "Yes"),
        ("N", "No")
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name="CASE Updated Date")

    State = models.CharField(max_length=100, default="MH")
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="--")
    CaseNumber = models.ForeignKey(Profile, on_delete=models.CASCADE)

    court_case_response_given = models.CharField(max_length=5, choices=court_case_responded,
                                                 default="Y", verbose_name='Court Case Responded or not ??')
    document_submitted = models.CharField(max_length=5, choices=reply_mode,
                                          default="writing", verbose_name='Reply Given in Writing or Bond??')

    CasesDocumentsDate = models.DateField(verbose_name='If Yes, Date of Document Submitted', blank=True, null=True)

    Document = models.FileField(verbose_name='Upload Affadavit Document here', blank=True, null=True)

    DocumentName = models.CharField(max_length=60, blank=True, null=True,
                                    verbose_name='Mention all Necessary Document Submitted')

    def save(self, *args, **kwargs):
        super(CourtCasesAffadavit, self).save(*args, **kwargs)
        filename = self.Document.url
        # CourtCasesAffadavit.objects.all().filter(CaseNumber_id=3).update(user=4)

    def __str__(self):
        return '%s -- %s' % (self.CaseNumber, self.DocumentName)

    class Meta:
        verbose_name = "Court Cases Affidavit"
        verbose_name_plural = "Court Cases Affidavit"


class CourtDecision(models.Model):

    Appeared1 = 'Appeared'
    Written1 = 'Written'
    Evidence1 = 'Evidence'
    Hearing1 = 'Hearing'
    Interim1 = 'Interim Order'
    order1 = 'order'

    court_action = [
        (Appeared1, 'Appeared'),
        (Written1, 'Written Statement'),
        (Evidence1, 'Evidence'),
        (Hearing1, 'Hearing'),
        (Interim1, 'Interim Order'),
        (order1, 'order')
    ]

    # Why do you need above variables? it could be simpler like below
    # court_action = [
    #     ('Appeared', 'Appeared'),
    #     ('Written Statement', 'Written Statement'),
    # ]

    # However, court actions are limited set of possibilities which can be standardized.
    # They could be stored inside a separate table and used in this model via a foreign key field
    # So decision_taken would be a ForeignKey() instead of CharField()
    # It will make these options easily modifiable

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name="CASE Updated Date")

    State = models.CharField(max_length=100, default="MH")
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="--")

    CaseNumber = models.ForeignKey(CourtCasesAffadavit, on_delete=models.CASCADE)
    action_date = models.DateField(verbose_name='Official Court Date-')
    decision_taken = models.CharField(max_length=50, choices=court_action,
                                      default="Appeared", verbose_name='Court Decision-')

    decision_details = models.CharField(max_length=200, blank=True, null=True, default="Court Have Given Direction")
    
    # Same court could have multiple hearings on the same day
    # if time is also of importance, datetimefield would be better here.
    court_next_date = models.DateField(verbose_name='Court Next Date-')

    # Does it need to be stored?? reality is different from expectation, nobody knows what the judge will decide
    next_expected_decision = models.CharField(max_length=200, blank=True, null=True, default="Court Next Expected Action-")

    def __str__(self):
        return '%s %s' % (self.CaseNumber, self.decision_taken)

    class Meta:
        verbose_name = "Court Decision"
        verbose_name_plural = "Court Decisions"


    # RCS1 = "Regular_Civil_Suit"
    # SCS1 = "Special_Civil_Suit"
    # RCA1 = "Regular Civil Appeal"
    # MCA1 = "Miscellaneous Civil Appeal"
    # CMA1 = "Civil Miscellaneous Application"
    # LAR1 = "Land Acquisation Reference"
    # SED1 = "Special Execution Darkhast"
    # # AC1 = "Arbitration Case"

    # SuitType_Choice = [
    #        (RCS1, 'Regular Civil Suit'),
    #        (SCS1, 'Special Civil Suit'),
    #        (RCA1, 'Regular Civil Appeal'),
    #        (MCA1,'Miscellaneous Civil Appeal'),
    #        (CMA1,'Civil Miscellaneous Application'),
    #        (LAR1,'Land Acquisation Reference'),
    #        (SED1,'Special Execution Darkhast'),
    #        # (AC1,’Arbitration Case’),
    #   ]
    # Country =models.CharField(max_length=200,default="District Court Parbhani")
    # CourtType =models.CharField(max_length=200,default="District Court Parbhani")
    # SuitType= models.CharField(max_length=200,choices=SuitType_Choice,
        # default="Regular Civil Suit",verbose_name='--Suit Type-')
